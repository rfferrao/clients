<?php
//
// Nagios Cross-Platform Agent (NCPA) Config Wizard
// Copyright (c) 2014-2021 Nagios Enterprises, LLC. All rights reserved.
//

include_once(dirname(__FILE__) . '/../configwizardhelper.inc.php');
require_once(dirname(__FILE__) . '/../../../config/deployment/includes/utils-deployment.inc.php');


ncpa_configwizard_init();


function ncpa_configwizard_init()
{
    $name = "ncpa";
    $args = array(
        CONFIGWIZARD_NAME => $name,
        CONFIGWIZARD_VERSION => "3.0.11",
        CONFIGWIZARD_TYPE => CONFIGWIZARD_TYPE_MONITORING,
        CONFIGWIZARD_DESCRIPTION => _("Monitor a host (Windows, Linux, OS X, Solaris, or AIX) using the Nagios Cross-Plaftorm Agent.") . ' <div class="hide">centos rhel sles suse opensuse oracle cloudlinux ubuntu debian redhat mac</div>',
        CONFIGWIZARD_DISPLAYTITLE => _("NCPA"),
        CONFIGWIZARD_FUNCTION => "ncpa_configwizard_func",
        CONFIGWIZARD_PREVIEWIMAGE => "ncpa.png",
        CONFIGWIZARD_FILTER_GROUPS => array('nagios','windows','linux','otheros'),
        CONFIGWIZARD_REQUIRES_VERSION => 5700
    );
    register_configwizard($name, $args);
}


/**
 * @param string $mode
 * @param null   $inargs
 * @param        $outargs
 * @param        $result
 *
 * @return string
 */
function ncpa_configwizard_func($mode = "", $inargs = null, &$outargs, &$result, $extra = array())
{
    $wizard_name = "ncpa";

    // Initialize return code and output
    $result = 0;
    $output = "";

    // Initialize output args - pass back the same data we got
    $outargs[CONFIGWIZARD_PASSBACK_DATA] = $inargs;

    switch ($mode) {

        case CONFIGWIZARD_MODE_GETSTAGE1HTML:
            $ip_address = grab_array_var($inargs, "ip_address", "");
            $port = grab_array_var($inargs, "port", "5693");
            $token = grab_array_var($inargs, "token", "");
            $no_ssl_verify = grab_array_var($inargs, "no_ssl_verify", 1);
            $os = grab_array_var($inargs, "os", grab_array_var($extra, "os", ""));

            // Only display the operating system selection if no os pre-selected by the wizard
            // this is for windows server and windows desktop mostly...
            $select_os = '';
            if (!array_key_exists('os', $extra)) {
                $select_os = '<tr>
                    <td class="vt"><label>' . _('System') . ':</label></td>
                    <td>
                        <select name="os" class="form-control">
                            <option value="" '.is_selected($os, "").'>'._('Generic').'</option>
                            <option value="centos" '.is_selected($os, "centos").'>CentOS</option>
                            <option value="rhel" '.is_selected($os, "rhel").'>RHEL</option>
                            <option value="oracle" '.is_selected($os, "oracle").'>Oracle</option>
                            <option value="ubuntu" '.is_selected($os, "ubuntu").'>Ubuntu</option>
                            <option value="debian" '.is_selected($os, "debian").'>Debian</option>
                            <option value="suse" '.is_selected($os, "suse").'>OpenSUSE</option>
                            <option value="sles" '.is_selected($os, "sles").'>SLES</option>
                            <option value="windows" '.is_selected($os, "windows").'>Windows</option>
                            <option value="osx" '.is_selected($os, "osx").'>Mac OS X</option>
                            <option value="sol" '.is_selected($os, "sol").'>Solaris</option>
                            <option value="aix" '.is_selected($os, "aix").'>AIX</option>
                        </select>
                        <div class="subtext">' . _('Used to set the icon for the host') . '.</div>
                    </td>
                </tr>';
            }

            $output = '
            <h5 class="ul">' . _('Setup NCPA') . '</h5>
            <p><i>' . _('The agent should be installed before you continue running this wizard.') . '</i></p>
            <ul style="padding: 0 0 0 30px;">
                <li><a href="https://www.nagios.org/ncpa#downloads" target="_blank">' . _('Download the latest version of NCPA') . '</a> ' . _('for the system you would like to monitor') . '</li>
                <li>' . _('Follow the') . ' <a href="https://www.nagios.org/ncpa/getting-started.php" target="_blank">' . _('installation instructions') . '</a> (<a href="https://assets.nagios.com/downloads/ncpa/docs/Installing-NCPA.pdf" target="_blank">' . _('PDF version') . '</a>) ' . _('and configure the token for the agent') . '</li>
            </ul>

            <h5 class="ul">' . _('Connect to NCPA') . '</h5>
            <table class="table table-condensed table-no-border table-auto-width table-padded">
                <tr>
                    <td class="vt"><label>' . _('Address') . ':</label></td>
                    <td>
                        <input type="text" size="40" name="ip_address" value="' . encode_form_val($ip_address) . '" class="textfield usermacro-detection form-control" autocomplete="off">
                        <div class="subtext">' . _('The IP address or FQDNS name used to connect to NCPA') . '.</div>
                    </td>
                </tr>
                <tr>
                    <td class="vt"><label>' . _('Port') . ':</label></td>
                    <td>
                        <input type="text" size="5" name="port" value="' . encode_form_val($port) . '" class="textfield usermacro-detection form-control">
                        <div class="subtext">' . _('Port used to connect to NCPA. Defaults to port 5693') . '.</div>
                    </td>
                </tr>
                <tr>
                    <td class="vt"></td>
                    <td class="checkbox">
                        <label>
                            <input type="checkbox" name="no_ssl_verify" value="1" ' . is_checked($no_ssl_verify, 1) . '>
                            ' . _("Do not verify SSL certificate") . '
                        </label>
                    </td>
                </tr>
                <tr>
                    <td class="vt"><label>' . _('Token') . ':</label></td>
                    <td>
                        <input type="password" size="15" name="token" id="token" value="' . encode_form_val($token) . '" class="textfield usermacro-detection form-control" autocomplete="off">
                        <button type="button" style="vertical-align: top;" class="btn btn-sm btn-default tt-bind btn-show-password" title="'._("Show password").'"><i class="fa fa-eye"></i></button>
                        <div class="subtext">' . _('Authentication token used to connect to NCPA') . '.</div>
                    </td>
                </tr>
                '.$select_os.'
            </table>';
            break;

        case CONFIGWIZARD_MODE_VALIDATESTAGE1DATA:
            // Get variables that were passed to us
            $ip_address = grab_array_var($inargs, "ip_address", "");
            $port = grab_array_var($inargs, "port", "5693");
            $token = grab_array_var($inargs, "token", "");
            $no_ssl_verify = grab_array_var($inargs, "no_ssl_verify", 1);

            // For multi-host ncpa run (from deployment)
            $hosts = grab_request_var("hosts", array());
            $job_id = grab_request_var("job_id", 0);

            // Grab deploy id if it exists
            $deploy_id = grab_request_var('deploy_id', 0);
            if (!empty($deploy_id)) {
                $agent = deploy_get_agent($deploy_id);
                $metadata = json_decode(base64_decode($agent['metadata']), true);
                $token = decrypt_data($metadata['token']);
                $ip_address = $agent['address'];
            }

            // Check for errors
            $errors = 0;
            $errmsg = array();

            if (have_value($ip_address) == false && empty($hosts)) {
                $errmsg[$errors++] = _("No address specified.");
            }
            if (have_value($port) == false) {
                $errmsg[$errors++] = _("No port number specified.");
            }

            // Test the connection if no errors
            if (empty($errors) && empty($hosts)) {

                $ip_address_replaced = nagiosccm_replace_user_macros($ip_address);
                $port_replaced = nagiosccm_replace_user_macros($port);
                $token_replaced = nagiosccm_replace_user_macros($token);

                // The URL we will use to query the NCPA agent, and do a walk
                // of all monitorable items.
                $query_url = "https://{$ip_address}:{$port}/testconnect/?token=".urlencode($token);
                $query_url_replaced = "https://{$ip_address_replaced}:{$port_replaced}/testconnect/?token=".urlencode($token_replaced);

                // Remove SSL verification or not
                $context = array("ssl" => array("verify_peer" => true, "verify_peer_name" => true));
                if ($no_ssl_verify) {
                    $context['ssl']['verify_peer'] = false;
                    $context['ssl']['verify_peer_name'] = false;
                }

                // All we want to do is test if we can hit this URL.
                $raw_json = file_get_contents($query_url_replaced, false, stream_context_create($context));
                if (empty($raw_json)) {
                    $errmsg[$errors++] = _("Unable to contact server at") . " {$query_url}.";
                } else {
                    $json = json_decode($raw_json, true);
                    if (!array_key_exists('value', $json)) {
                        $errmsg[$errors++] = _("Bad token for connection.");
                    }
                }

            }

            if ($errors > 0) {
                $outargs[CONFIGWIZARD_ERROR_MESSAGES] = $errmsg;
                $result = 1;
            }
            break;

        case CONFIGWIZARD_MODE_GETSTAGE2HTML:
            // Get variables that were passed to us
            $ip_address = grab_array_var($inargs, "ip_address", "");
            $port = grab_array_var($inargs, "port", "5693");
            $token = grab_array_var($inargs, "token", "");
            $no_ssl_verify = grab_array_var($inargs, "no_ssl_verify", 1);
            $hostname = grab_array_var($inargs, 'hostname', @gethostbyaddr($ip_address));
            $default_mem_units = grab_array_var($inargs, 'default_mem_units', 'Gi');

            // Set os to extra os
            $os = grab_array_var($inargs, "os", grab_array_var($extra, "os", ""));

            // For multi-host ncpa run (from deployment)
            $hosts = grab_request_var("hosts", array());
            $job_id = grab_request_var("job_id", 0);
            if (!empty($job_id)) {
                if (count($hosts) == 1) {
                    $job = deploy_get_job($job_id);
                    $token = decrypt_data($job['ncpa_token']);
                    $ip_address = $hosts[0];
                    $hostname = gethostbyaddr($ip_address);
                    $hosts = array();
                    $os = $job['os'];
                } else {
                    // Don't set the normal things because it's multi-host...
                }
            }

            // Grab deploy id if it exists
            $deploy_id = grab_request_var('deploy_id', 0);
            if (!empty($deploy_id)) {
                $agent = deploy_get_agent($deploy_id);
                $metadata = json_decode(base64_decode($agent['metadata']), true);
                $token = decrypt_data($metadata['token']);
                $ip_address = $agent['address'];
                $hostname = $agent['hostname'];
                $os = $agent['os'];
            }

            $rp_address = nagiosccm_replace_user_macros($ip_address);
            $rp_port = nagiosccm_replace_user_macros($port);
            $rp_token = nagiosccm_replace_user_macros($token);

            $services = grab_array_var($inargs, "services", array());
            $services_serial = grab_array_var($inargs, "services_serial", "");
            if ($services_serial) {
                $services = json_decode(base64_decode($services_serial), true);
            }

            $categories = array();
            $root = array();

            // If we are running the wizard on a single host, get the proper data
            // otherwise we need to use generic data
            if (empty($hosts)) {

                $api_url = "https://{$rp_address}:{$rp_port}/api";
                $interface_data = ncpa_configwizard_get_api_data('interface', $api_url, $rp_token, $no_ssl_verify);
                $disk_data = ncpa_configwizard_get_api_data('disk', $api_url, $rp_token, $no_ssl_verify);
                $service_data = ncpa_configwizard_get_api_data('services', $api_url, $rp_token, $no_ssl_verify);
                $process_data = ncpa_configwizard_get_api_data('processes', $api_url, $rp_token, $no_ssl_verify);
                $plugin_data = ncpa_configwizard_get_api_data('plugins', $api_url, $rp_token, $no_ssl_verify);

                $cpu_data = ncpa_configwizard_get_api_data('cpu/percent', $api_url, $rp_token, $no_ssl_verify, array('aggregate' => 'avg'));
                $user_data = ncpa_configwizard_get_api_data('user/count', $api_url, $rp_token, $no_ssl_verify);
                $mem_data = ncpa_configwizard_get_api_data('memory/virtual/percent', $api_url, $rp_token, $no_ssl_verify);
                $swap_data = ncpa_configwizard_get_api_data('memory/swap/percent', $api_url, $rp_token, $no_ssl_verify);

            } else {
                $disk_data = array('' => '');
                $interface_data = array('' => '');
                $service_data = array();
                $process_data = array();
                $plugin_data = array();
            }

            $root['disk'] = $disk_data;
            $root['interface'] = $interface_data;
            $root['services'] = $service_data;

            // Set up processes
            $tmp = array();
            foreach ($process_data as $p) {
                if (array_key_exists($p['name'], $tmp)) {
                    $tmp[$p['name']]++;
                } else {
                    $tmp[$p['name']] = 1;
                }
            }
            $root['processes'] = $tmp;

            // Sort plugin data
            if (!empty($plugin_data)) {
                sort($plugin_data);
            }
            $root['plugins'] = $plugin_data;

            $icon = _("Generic");
            if (!empty($os)) {
                $icon = '<img src="' . nagioscore_get_ui_url() . "images/logos/" . npca_configwizard_get_os_icon($os) . '" style="">';
            }

            $output = '
            <input type="hidden" name="ip_address" value="' . encode_form_val($ip_address) . '">
            <input type="hidden" name="port" value="' . encode_form_val($port) . '">
            <input type="hidden" name="token" value="' . encode_form_val($token) . '">
            <input type="hidden" name="no_ssl_verify" value="' . intval($no_ssl_verify) . '">
            <input type="hidden" name="os" value="' . encode_form_val($os) . '">
            <input type="hidden" name="job_id" value="' . intval($job_id) . '">';

            if (!empty($hosts)) {

                $output .= ' <h5 class="ul">' . _("Host Information") . '</h5>
                            <p>' . _('Adding the following service checks to the below hosts.') . '</p>
                            <table class="table table-condensed table-bordered table-striped table-auto-width table-padded">
                                <thead>
                                    <tr>
                                        <th>' . _('Hostname') . '</th>
                                    </tr>
                                </thead>
                                <tbody>';

                    foreach ($hosts as $host) {
                        $output .= '<tr>';
                        $output .= '<td><input type="hidden" name="hosts[]" value="'.encode_form_val($host).'">' . encode_form_val($host) . '</td>';
                        $output .= '</tr>';
                    }

                $output .= '     </tbody>
                            </table>';

            } else {

                $output .= '<h5 class="ul">' . _("Host Information") . '</h5>
                <table class="table table-condensed table-no-border table-auto-width table-padded">
                    <tr>
                        <td class="vt"><label>' . _("Address") . ':</label></td>
                        <td>
                            <input type="text" size="20" value="' . encode_form_val($ip_address) . '" class="textfield form-control" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td class="vt"><label>' . _("Host Name") . ':</label></td>
                        <td>
                            <input type="text" size="20" name="hostname" id="hostname" value="' . encode_form_val($hostname) . '" class="textfield form-control">
                            <div class="subtext">' . _("The hostname you'd like to have associated with this host") . '.</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="vt"><label>' . _("Port") . ':</label></td>
                        <td>
                            <input type="text" size="5" value="' . encode_form_val($port) . '" class="textfield form-control" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td style="vt">
                            <label>' . _('System') . ':</label>
                        </td>
                        <td>
                            '.$icon.'
                        </td>
                    </tr>
                </table>';

            }

            $defaults = grab_array_var($extra, "defaults", "");

            // Set defaults for services
            $default_services['cpu_usage']['monitor'] = 'on';
            $default_services['cpu_usage']['warning'] = 85;
            $default_services['cpu_usage']['critical'] = 95;
            $default_services['cpu_usage']['average'] = 1;
            $default_services['users']['monitor'] = 'off';
            $default_services['users']['warning'] = 2;
            $default_services['users']['critical'] = 4;
            $default_services['memory_usage']['monitor'] = 'on';
            $default_services['memory_usage']['warning'] = 85;
            $default_services['memory_usage']['critical'] = 95;
            $default_services['swap_usage']['monitor'] = 'off';
            $default_services['swap_usage']['warning'] = 5;
            $default_services['swap_usage']['critical'] = 10;

            if (isset($root['disk']['logical'])) {
                $id = 0;
                foreach ($root['disk']['logical'] as $title => $value) {
                    if (empty($value['device_name'])) {
                        continue;
                    }
                    $default_services['disk'][$id]['monitor'] = 'on';
                    $default_services['disk'][$id]['warning'] = 80;
                    $default_services['disk'][$id]['critical'] = 90;
                    $default_services['disk'][$id]['name'] = $value['device_name'];
                    if (is_array($value['device_name'])) {
                        $default_services['disk'][$id]['name'] = $value['device_name'][0];
                    }
                    $default_services['disk'][$id]['title'] = $title;
                    $id++;
                }
            } else if (!empty($hosts)) {
                $default_services['disk'][0]['monitor'] = 'on';
                $default_services['disk'][0]['warning'] = 80;
                $default_services['disk'][0]['critical'] = 90;
                $default_services['disk'][0]['name'] = "/";
            }

            ksort($root['interface']);
            $id = 0;
            foreach ($root['interface'] as $title => $value) {
                if (stripos($title, "Local Area Connection") !== false || stripos($title, "eth") !== false || stripos($title, "ens") !== false || stripos($title, "Wireless") !== false) {
                    $default_services['interface'][$id]['monitor'] = 'on';
                } else {
                    $default_services['interface'][$id]['monitor'] = 'off';
                }
                $default_services['interface'][$id]['warning'] = 90;
                $default_services['interface'][$id]['critical'] = 100;
                $default_services['interface'][$id]['name'] = $title;
                $id++;
            }

            // Create defaults for processes
            if ($defaults == "desktop") {

                // Add explorer
                $default_services['process'][] = array(
                    'monitor' => 'off',
                    'name' => 'explorer.exe',
                    'desc' => 'Explorer',
                    'warning' => '',
                    'critical' => '1:'
                );

            }

            $default_services['process'][] = array(
                'monitor' => 'off',
                'name' => '',
                'desc' => '',
                'warning' => '',
                'critical' => ''
            );

            // Create defaults for services
            if ($defaults == "desktop" || $defaults == "server") {

                // MSSQL server defaults
                if (array_key_exists('MSSQLSERVER', $root['services'])) {
                    $default_services['services'][] = array(
                        'monitor' => 'off',
                        'name' => 'MSSQLSERVER',
                        'desc' => 'MSSQL Service Status',
                        'state' => 'running'
                    );
                } else if (array_key_exists('MSSQL$SQLEXPRESS', $root['services'])) {
                    $default_services['services'][] = array(
                        'monitor' => 'off',
                        'name' => 'MSSQL$SQLEXPRESS',
                        'desc' => 'MSSQL Express Service Status',
                        'state' => 'running'
                    );
                }

                // Windows Defender running
                if (array_key_exists('WinDefend', $root['services'])) {
                    $default_services['services'][] = array(
                        'monitor' => 'off',
                        'name' => 'WinDefend',
                        'desc' => 'Windows Defender Service',
                        'state' => 'running'
                    );
                }

                // MSSQL telemetry defaults
                if (array_key_exists('MSSQLTELEMETRY', $root['services'])) {
                    $default_services['services'][] = array(
                        'monitor' => 'off',
                        'name' => 'MSSQLTELEMETRY',
                        'desc' => 'MSSQL Telemetry Service Status',
                        'state' => 'running'
                    );
                } else if (array_key_exists('SQLTELEMETRY$SQLEXPRESS', $root['services'])) {
                    $default_services['services'][] = array(
                        'monitor' => 'off',
                        'name' => 'SQLTELEMETRY$SQLEXPRESS',
                        'desc' => 'MSSQL Express Telemetry Service Status',
                        'state' => 'running'
                    );
                }

                // IIS server defaults
                if ($defaults == "server") {
                    if (array_key_exists('WAS', $root['services'])) {
                        $default_services['services'][] = array(
                            'monitor' => 'off',
                            'name' => 'WAS',
                            'desc' => 'Windows Process Activation Service Status',
                            'state' => 'running'
                        );
                    }
                    if (array_key_exists('W3SVC', $root['services'])) {
                        $default_services['services'][] = array(
                            'monitor' => 'off',
                            'name' => 'W3SVC',
                            'desc' => 'IIS Web Publishing Service Status',
                            'state' => 'running'
                        );
                    }
                }

            }

            $default_services['services'][] = array(
                'monitor' => 'off',
                'name' => '',
                'desc' => '',
                'state' => 'running'
            );

            // Create defaults for counters
            if ($defaults == "desktop" || $defaults == "server") {
                $default_services['counters'][] = array(
                    'monitor' => 'off',
                    'name' => '\Paging File(*)\% Usage',
                    'desc' => _('Paging File Usage'),
                    'sleep' => '',
                    'warning' => 80,
                    'critical' => 90
                );
                $default_services['counters'][] = array(
                    'monitor' => 'off',
                    'name' => '\Event Log\Events/sec',
                    'desc' => _('Event Log Events/sec'),
                    'sleep' => 5,
                    'warning' => 10,
                    'critical' => 20
                );
                $default_services['counters'][] = array(
                    'monitor' => 'off',
                    'name' => '\Terminal Services\Active Sessions',
                    'desc' => _('Active Terminal Sessions'),
                    'sleep' => '',
                    'warning' => 2,
                    'critical' => 3
                );
                $default_services['counters'][] = array(
                    'monitor' => 'off',
                    'name' => '\System\Threads',
                    'desc' => _('Number of System Threads'),
                    'sleep' => '',
                    'warning' => '',
                    'critical' => ''
                );
                if ($defaults == "desktop") {
                    $default_services['counters'][] = array(
                        'monitor' => 'off',
                        'name' => '\Print Queue(*)\Job Errors',
                        'desc' => _('Print Queue Job Errors'),
                        'sleep' => '',
                        'warning' => 1,
                        'critical' => 2
                    );
                    $default_services['counters'][] = array(
                        'monitor' => 'off',
                        'name' => '\Telephony\Active Lines',
                        'desc' => _('Active Telephony Lines'),
                        'sleep' => '',
                        'warning' => '',
                        'critical' => ''
                    );
                } else {
                    $default_services['counters'][] = array(
                        'monitor' => 'off',
                        'name' => '\Server\Errors Logon',
                        'desc' => _('Logon Errors'),
                        'sleep' => '',
                        'warning' => 2,
                        'critical' => 20
                    );
                    $default_services['counters'][] = array(
                        'monitor' => 'off',
                        'name' => '\Server Work Queues(0)\Queue Length',
                        'desc' => _('Server Work Queues'),
                        'sleep' => '',
                        'warning' => 4,
                        'critical' => 7
                    );
                }
            }

            $default_services['counters'][] = array(
                    'monitor' => 'off',
                    'name' => '',
                    'desc' => '',
                    'sleep' => '',
                    'warning' => '',
                    'critical' => ''
                );

            // Create only one plugin if no plugins saved
            $default_services['plugins'][0]['monitor'] = 'off';
            $default_services['plugins'][0]['name'] = '';
            $default_services['plugins'][0]['desc'] = '';
            $default_services['plugins'][0]['args'] = '';

            if (empty($services)) {
                $services = $default_services;
            }

            $output .= '
            <script src="../includes/configwizards/ncpa/ncpa.inc.js"></script>
            <h5 class="ul">' . _("System Metrics") . '</h5>
            <p>' . _("Specify the metrics you'd like to monitor on the NCPA Agent") . '.</p>
            <table class="table table-no-border table-auto-width table-padded">
                <tr>
                    <td class="vt">
                        <input type="checkbox" class="checkbox" name="services[cpu_usage][monitor]" id="cpum" ' . is_checked(grab_array_var($services['cpu_usage'], "monitor"), "on") . '>
                    </td>
                    <td class="vt">
                        <div>
                            <label for="cpum" style="line-height: auto;">
                                <b>' . _('CPU Usage') . '</b>
                            </label>
                            <div style="margin-bottom: 6px;">' . _('Check the CPU usage of the system') . '.</div>
                        </div>
                        <div>
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="1" name="services[cpu_usage][warning]" value="' . encode_form_val($services['cpu_usage']['warning']) . '" class="form-control condensed"> %&nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="1" name="services[cpu_usage][critical]" value="' . encode_form_val($services['cpu_usage']['critical']) . '" class="form-control condensed"> %
                        </div>
                        <div class="checkbox" style="margin-top: 5px;">
                            <label>
                                <input type="checkbox" name="services[cpu_usage][average]" value="1" ' . is_checked($services['cpu_usage']['average'], 1) . '> ' . _("Show average CPU usage instead of per cpu core") . '
                            </label>
                        </div>
                    </td>';

                    if (!empty($cpu_data)) {
                        $output .= '
                        <td class="vt form-inline">
                            <div><label>' . _('Current CPU Usage') . '</label></div>
                            <div style="margin-top: 4px;">
                                <div class="input-group">
                                    <input type="text" style="width: 50px; height: 26px;" value="' . floatval($cpu_data[0][0]) . '" class="form-control cpu condensed">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </td>';
                    }

                    $output .= '
                </tr>
                <tr>
                    <td class="vt">
                        <input type="checkbox" class="checkbox" name="services[users][monitor]" id="ucount" ' . is_checked(grab_array_var($services['users'], "monitor"), "on") . '>
                    </td>
                    <td class="vt">
                        <div>
                            <label for="ucount" style="line-height: auto;">
                                <b>' . _('User Count') . '</b>
                            </label>
                            <div style="margin-bottom: 6px;">' . _('Check the number of users currently logged into the system') . '.</div>
                        </div>
                        <div>
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="1" name="services[users][warning]" value="' . encode_form_val($services['users']['warning']) . '" class="form-control condensed"> #&nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="1" name="services[users][critical]" value="' . encode_form_val($services['users']['critical']) . '" class="form-control condensed"> #
                        </div>
                    </td>';

                    if (!empty($user_data)) {
                        $output .= '
                        <td>
                            <div><label>' . _('Current User Count') . '</label></div>
                            <div style="margin-top: 4px;">
                                <input type="text" style="width: 60px; height: 26px;" value="' . $user_data[0] . '" class="form-control condensed">
                            </div>
                        </td>';
                    }

                    $output .= '
                </tr>
            </table>
            
            <h5 class="ul">' . _('Memory Metrics') . '</h5>
            <p>
                ' . _('Default units to use for memory metric ouput') . ': 
                <select name="default_mem_units" class="form-control">
                    <option value="b" ' . is_selected($default_mem_units, 'b') . '>b</option>
                    <option value="B" ' . is_selected($default_mem_units, 'B') . '>B</option>
                    <option value="k" ' . is_selected($default_mem_units, 'k') . '>k</option>
                    <option value="Ki" ' . is_selected($default_mem_units, 'Ki') . '>Ki</option>
                    <option value="G" ' . is_selected($default_mem_units, 'G') . '>G</option>
                    <option value="Gi" ' . is_selected($default_mem_units, 'Gi') . '>Gi</option>
                    <option value="T" ' . is_selected($default_mem_units, 'T') . '>T</option>
                    <option value="Ti" ' . is_selected($default_mem_units, 'Ti') . '>Ti</option>
                </select>
            </p>
            <table class="table table-no-border table-auto-width table-padded">
                <tr>
                    <td class="vt">
                        <input type="checkbox" class="checkbox" name="services[memory_usage][monitor]" id="mainm" ' . is_checked(grab_array_var($services['memory_usage'], 'monitor'), 'on') . '>
                    </td>
                    <td>
                        <label class="normal" for="mainm">
                            <b>' . _('Main Memory Usage') . '</b><br>
                            ' . _('Monitor memory usage as percentage of memory used') . '.
                        </label>
                        <div class="pad-t5">
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="1" name="services[memory_usage][warning]" value="' . encode_form_val($services['memory_usage']['warning']) . '" class="form-control condensed"> %&nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="1" name="services[memory_usage][critical]" value="' . encode_form_val($services['memory_usage']['critical']) . '" class="form-control condensed"> %
                        </div>
                    </td>';

                if (!empty($mem_data)) {
                    $output .= '
                        <td class="vt form-inline">
                            <div><label>' . _('Current Memory Usage') . '</label></div>
                            <div style="margin-top: 4px;">
                                <div class="input-group">
                                    <input type="text" style="width: 50px; height: 26px;" value="' . floatval($mem_data[0]) . '" class="form-control condensed">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </td>';
                }

                $output .= '
                </tr>
                <tr>
                    <td class="vt">
                        <input type="checkbox" class="checkbox" name="services[swap_usage][monitor]" id="swapm" ' . is_checked(grab_array_var($services['swap_usage'], 'monitor'), 'on') . '>
                    </td>
                    <td>
                        <label class="normal" for="swapm">
                            <b>' . _('Swap Usage') . '</b><br>
                            ' . _('Monitor the percentage of allocated swap used') . '.
                        </label>
                        <div class="pad-t5">
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="1" name="services[swap_usage][warning]" value="' . encode_form_val($services['swap_usage']['warning']) . '" class="form-control condensed"> %&nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="1" name="services[swap_usage][critical]" value="' . encode_form_val($services['swap_usage']['critical']) . '" class="form-control condensed"> %
                        </div>
                    </td>';

                if (!empty($swap_data)) {
                    $output .= '
                        <td class="vt form-inline">
                            <div><label>' . _('Current Swap Usage') . '</label></div>
                            <div style="margin-top: 4px;">
                                <div class="input-group">
                                    <input type="text" style="width: 50px; height: 26px;" value="' . $swap_data[0] . '" class="form-control condensed">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </td>';
                }

            $curr_usage = '';
            if (empty($hosts)) {
                $curr_usage = '<th><label>'._("Current Usage").'</label></th>';
            }

            $output .= '
                </tr>
            </table>
    
            <h5 class="ul">' . _("Disk Metrics") . '</h5>
            <p>' . _("Specify the disks the the warning and critical percentages for disk capacity") . '.</p>';

            if (!empty($hosts)) {
                $output .= '<p><b>'._('Disk checks specified will be applied to all hosts. If the disk name does not exist it will show CRITICAL.').'</b></p>';
            }

            $output .= '
            <table class="table table-condensed table-no-border table-auto-width table-padded" id="disk-metrics-table">
                <thead>
                    <tr>
                        <th></th>
                        <th><label>'._("Disk/Mount").'</label></th>
                        '.$curr_usage.'
                        <th><label>'._("Thresholds").'</label></th>
                    </tr>
                </thead>
                <tbody class="form-inline" id="disk-list">';

            $disabled = ' readonly';
            if (!empty($hosts)) {
                $disabled = '';
            }

            $id = 0;
            foreach ($services['disk'] as $id => $metrics) {
                $title = !empty($metrics['title']) ? $metrics['title'] : $metrics['name'];
                $titleout = str_replace('|', '\\', $title);

                $disk_usage = "";
                if (array_key_exists('total', $root['disk']['logical'][$title])) {
                    $disk_total = $root['disk']['logical'][$title]['total'][0].' '.$root['disk']['logical'][$title]['total'][1].' '._('Total');
                    $disk_usage = '<td><div class="input-group"><input type="text" class="form-control" style="width: 50px;" value="'.floatval($root['disk']['logical'][$title]['used_percent'][0]).'"><div class="input-group-addon">% &nbsp;<i class="fa fa-question-circle fa-14 tt-bind" title="'.encode_form_val($disk_total).'"></i></div></div></td>';
                }

                $output .= '
                <tr>
                    <td>
                        <input type="checkbox" id="d' . $id . '" class="checkbox" name="services[disk][' . $id . '][monitor]" ' . is_checked(grab_array_var($services['disk'][$id], 'monitor'), 'on') . '>
                        <input type="hidden" name="services[disk]['.$id.'][title]" value="'.encode_form_val($title).'">
                    </td>
                    <td>
                        <input type="hidden" name="services[disk][' . $id . '][name]" value="' . encode_form_val($titleout) . '">
                        <label for="d' . $id . '"><input type="text" class="form-control" name="services[disk][' . $id . '][name]" value="' . encode_form_val($titleout) . '"'.$disabled.'></label>
                    </td>
                    '.$disk_usage.'
                    <td>
                        <span style="margin-left: 6px;">
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="1" name="services[disk][' . $id . '][warning]" value="' . encode_form_val($services['disk'][$id]['warning']) . '" class="form-control condensed"> %&nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="1" name="services[disk][' . $id . '][critical]" value="' . encode_form_val($services['disk'][$id]['critical']) . '" class="form-control condensed"> %
                        </span>
                    </td>
                </tr>';
                $id++;
            }

            $output .= '
                </tbody>
            </table>';

            if (!empty($hosts)) {
                $output .= '<p><a id="add-disk">'._("Add Disk Check").'</a></p>';
            }

            $output .= '<h5 class="ul">' . _('Network Interface Metrics') . '</h5>
            <p>' . _("Specify bandwidth limits for your network interfaces. Note that these measurements are per second, not a counter.") . '</p>';

            if (empty($hosts)) {
                $output .= '
                <script type="text/javascript">
                var IF_SHOW = 0;
                $(document).ready(function() {
                    $(".show-hidden-interfaces").click(function() {
                        if (!IF_SHOW) {
                            IF_SHOW = 1;
                            $(this).html("'._('Hide unselected interfaces').'");
                            $(".hidden-interface").removeClass("hide");
                        } else {
                            IF_SHOW = 0;
                            $(this).html("'._('Show all interfaces').'");
                            $(".hidden-interface").each(function() {
                                if (!$(this).find("input.checkbox").is(":checked")) {
                                    $(this).addClass("hide");
                                }
                            });
                        }
                    });
                });
                </script>';
            } else {
                $output .= '<p><b>'._('Interface checks specified will be applied to all hosts. If the interface name does not exist it will show CRITICAL.').'</b></p>';
            }

            $output .= '
            <table class="table table-condensed table-no-border table-auto-width table-padded">
                <thead>
                    <th></th>
                    <th><label>'._("Interface Name").'</label></th>
                    <th><label>'._("Thresholds").'</label></th>
                </thead>
                <tbody id="interfaces-list">';

            $disabled = ' readonly';
            if (!empty($hosts)) {
                $disabled = '';
            }

            $id = 0;
            $hidden = false;
            foreach ($services['interface'] as $id => $metrics) {
                $hide = '';
                $title = $metrics['name'];
                if (!is_checked(grab_array_var($services['interface'][$id], 'monitor'), 'on') && empty($hosts)) {
                    $hide = 'class="hidden-interface hide"';
                }

                $output .= '
                <tr '.$hide.'>
                    <td>
                        <input type="checkbox" id="ni' . $id . '" class="checkbox" name="services[interface][' . $id . '][monitor]" ' . is_checked(grab_array_var($services['interface'][$id], 'monitor'), 'on') . '>
                    </td>
                    <td>
                        <label for="ni' . $id . '"><input type="text" class="form-control" style="width: 300px;" name="services[interface][' . $id . '][name]" value="' . encode_form_val(str_replace("|", "/", $title)) . '"'.$disabled.'></label>
                    </td>
                    <td>
                        <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                        <input type="text" size="2" name="services[interface][' . $id . '][warning]" value="' . encode_form_val($services['interface'][$id]['warning']) . '" class="form-control condensed"> MB/s&nbsp;&nbsp;
                        <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                        <input type="text" size="2" name="services[interface][' . $id . '][critical]" value="' . encode_form_val($services['interface'][$id]['critical']) . '" class="form-control condensed"> MB/s
                    </td>
                </tr>';
                $id++;
            }

            $output .= '
                </tbody>
            </table>';

            if (empty($hosts)) {
                $output .= '<p><a class="show-hidden-interfaces">'._("Show all interfaces").'</a></p>';
            } else {
                $output .= '<p><a id="add-interface">'._("Add Interface Check").'</a></p>';
            }

            $output .= '
            <h5 class="ul">' . _("Services") . '</h5>
            <p>' . _("Specify which services should be running or stopped. Depending on the selected state you will recieve an OK when the process is in the selected state and a CRITICAL if the process is not in the state selected.") . '</p>
            <table class="table table-condensed table-no-border table-auto-width table-padded">
                <thead>
                    <tr>
                        <th></th>
                        <td><label>'._('Service Description').'</label></td>
                        <th><label>' . _("Service Name") . '</label></th>
                        <th><label>' . _("Expected Status") . '</label></th>
                    </tr>
                </thead>
                <tbody id="services-list">';

            foreach ($services['services'] as $i => $metrics) {
                $services_selector = '';
                if (empty($hosts)) {
                    $services_selector = '<div class="input-group-addon" style="padding-bottom: 5px;"><span style="cursor: pointer;" class="service-selector" data-id="' . $i . '"><img src="' . theme_image('history2.png') . '" title="' . _("Select service from currently running services list") . '"></span></div>';
                }
                $output .= '
                    <tr>
                        <td>
                            <input type="checkbox" name="services[services][' . $i . '][monitor]" ' . is_checked($metrics['monitor'], 'on') . '>
                        </td>
                        <td style="padding-right: 10px;">
                            <input type="text" style="width: 240px;" class="form-control" name="services[services][' . $i . '][desc]" value="' . encode_form_val($metrics['desc']) . '">
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control" style="width: 200px;" name="services[services][' . $i . '][name]" value="' . encode_form_val($metrics['name']) . '">
                                '.$services_selector.'
                            </div>
                        </td>
                        <td>
                            <label class="normal" style="margin-right: 6px;"><input name="services[services][' . $i . '][state]" style="vertical-align: text-bottom;" type="radio" value="running" ' . is_checked($metrics['state'], 'running') . '> ' . _("Running") . '</label><label class="normal"><input type="radio" style="vertical-align: text-bottom;" value="stopped" name="services[services][' . $i . '][state]" ' . is_checked($metrics['state'], 'stopped') . '> ' . _("Stopped") . '</label>
                        </td>
                    </tr>';
            }

            // Create a list of services for the JS
            $service_list = '';
            ksort($root['services']);
            foreach ($root['services'] as $service => $status) {
                $service_list .= '<option value="' . encode_form_val($service) . '" data-status="' . encode_form_val($status) . '">' . encode_form_val($service) . ' (' . encode_form_val($status) . ')</option>';
            }

            // Create a list of processes for the JS
            $processes_list = '';
            ksort($root['processes']);
            foreach ($root['processes'] as $process => $amount) {
                $processes_list .= '<option value="' . encode_form_val($process) . '" data-amount="' . encode_form_val($amount) . '">' . encode_form_val($process) . ' (' . encode_form_val($amount) . ' ' . _('running') . ')</option>';
            }

            // Create a list of plugins for the JS
            $plugin_list = '';
            if (!empty($root['plugins'])) {
                $plugin_list .= '<select id="selected-plugin" class="form-control">';
                foreach ($root['plugins'] as $plugin) {
                    $plugin_list .= '<option value="' . encode_form_val($plugin) . '">' . encode_form_val($plugin) . '</option>';
                }
                $plugin_list .= '</select>';
            } else {
                $plugin_list .= '<div class="well">'._('No plugins found on the agent system') . '</div>';
            }

            $output .= '
                </tbody>
            </table>
            <div style="margin: 10px 0 20px 0;">
                <a style="cursor: pointer;" id="add-new-service">'._('Add Another Service Check').'</a>
            </div>
    
            <h5 class="ul">' . _('Processes') . '</h5>
            <p>' . _("Specify which processes should be running and how many of them there should be.") . '</p>
            <table class="table table-condensed table-no-border table-auto-width table-padded">
                <thead>
                    <tr>
                        <td></td>
                        <td><label>'._('Service Description').'</label></td>
                        <td><label>'._('Process Name').'</label></td>
                        <td><label>'._('Thresholds (Process count)').'</label> <i class="fa fa-question-circle pop" data-content="' . sprintf(_('If a value should be warning or critical when it is 0, you can use the threshold <code>1:</code> which will trigger a warning or critical when the value is outside <code>1 to %s</code>.'), '∞') . '"></i></td>
                    </tr>
                </thead>
                <tbody id="process-list">';

            foreach ($services['process'] as $i => $metrics) {
                $select_process = '';
                if (empty($hosts)) {
                     $select_process = '<div class="input-group-addon" style="padding-bottom: 5px;">
                                        <span style="cursor: pointer;" class="process-selector" data-id="' . $i . '">
                                        <img src="' . theme_image('history2.png') . '" title="' . _("Select process from currently running process list") . '"></span>
                                    </div>';
                }
                $output .= '
                    <tr>
                        <td>
                            <input type="checkbox" class="checkbox" name="services[process][' . $i . '][monitor]" ' . is_checked($metrics['monitor'], 'on') . '>
                        </td>
                        <td style="padding-right: 10px;">
                            <input type="text" style="width: 240px;" class="form-control" name="services[process][' . $i . '][desc]" value="' . encode_form_val($metrics['desc']) . '">
                        </td>
                        <td style="padding-right: 10px;">
                            <div class="input-group">
                                <input type="text" class="destext form-control" name="services[process][' . $i . '][name]" value="' . encode_form_val($metrics['name']) . '">
                                '.$select_process.'
                            </div>
                        </td>
                        <td>
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" class="destext form-control" size="2" name="services[process][' . $i . '][warning]" value="' . encode_form_val($metrics['warning']) . '"> &nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" class="destext form-control" size="2" name="services[process][' . $i . '][critical]" value="' . encode_form_val($metrics['critical']) . '">
                        </td>
                    </tr>';
            }

            $process_selector = '';
            $service_selector = '';
            $plugin_selector = '';
            if (empty($hosts)) {
                $process_selector = '<div class="input-group-addon" style="padding-bottom: 5px;"><span style="cursor: pointer;" class="process-selector" data-id="\'+processnum+\'"><img src="' . theme_image('history2.png') . '" title="\'+"' . _("Select process from currently running process list") . '"+\'"></span></div>';
                $service_selector = '<div class="input-group-addon" style="padding-bottom: 5px;"><span style="cursor: pointer;" class="service-selector" data-id="\'+servicenum+\'"><img src="' . theme_image('history2.png') . '" title="\'+"' . _("Select service from currently running services list") . '"+\'"></span></div>';
                $plugin_selector = '<div class="input-group-addon" style="padding-bottom: 5px;"><span style="cursor: pointer;" class="plugin-selector" data-id="\'+pluginnum+\'"><img src="' . theme_image('history2.png') . '" title="\'+"' . _("Select a plugin from the list") . '"+\'"></span></div>';
            }

            $output .= '
                </tbody>
            </table>
            <div style="margin: 10px 0 20px 0;">
                <a style="cursor: pointer;" id="add-new-process">'._('Add Another Process Check').'</a>
            </div>
            <script type="text/javascript">
            var processnum = ' . (count($services['process']) - 1) . ';
            var servicenum = ' . (count($services['services']) - 1) . ';
            var counternum = ' . (count($services['counters']) - 1) . ';
            var pluginnum = ' . (count($services['plugins']) - 1) . ';
            var inum = '. (count($services['interface']) - 1) .';
            var dnum = '. (count($services['disk']) - 1) .';
            $(document).ready(function() {

                $("#add-disk").click(function() {
                    dnum++;
                    $("#disk-list").append(\'<tr><td><input type="checkbox" id="d\'+dnum+\'" class="checkbox" name="services[disk][\'+dnum+\'][monitor]"></td><td><input type="hidden" name="services[disk][\'+dnum+\'][name]" value=""><label for="d\'+dnum+\'"><input type="text" class="form-control" name="services[disk][\'+dnum+\'][name]" value=""></label></td><td><span style="margin-left: 6px;"><label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label> <input type="text" size="1" name="services[disk][\'+dnum+\'][warning]" value="80" class="form-control condensed"> %&nbsp;&nbsp; <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label> <input type="text" size="1" name="services[disk][\'+dnum+\'][critical]" value="90" class="form-control condensed"> %</span></td></tr>\');
                });

                $("#add-interface").click(function() {
                    inum++;
                    $("#interfaces-list").append(\'<tr><td><input type="checkbox" id="ni\'+inum+\'" class="checkbox" name="services[interface][\'+inum+\'][monitor]"></td><td><label for="ni\'+inum+\'"><input type="text" class="form-control" style="width: 300px;" name="services[interface][\'+inum+\'][name]" value=""></label></td><td><label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label> <input type="text" size="2" name="services[interface][\'+inum+\'][warning]" value="90" class="form-control condensed"> MB/s&nbsp;&nbsp; <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label> <input type="text" size="2" name="services[interface][\'+inum+\'][critical]" value="100" class="form-control condensed"> MB/s</td></tr>\');
                });

                $("#add-new-process").click(function() {
                    processnum++;
                    $("#process-list").append(\'<tr><td><input type="checkbox" class="checkbox" name="services[process][\'+processnum+\'][monitor]" /></td><td style="padding-right: 10px;"><input type="text" style="width: 240px;" class="destext form-control" name="services[process][\'+processnum+\'][desc]" value=""></td><td style="padding-right: 10px;"><div class="input-group"><input type="text" class="destext form-control" name="services[process][\'+processnum+\'][name]" value="">'.$process_selector.'</div></td><td><label><img src="'.theme_image('error.png').'" class="tt-bind" title="\'+"'._('Warning Threshold').'"+\'"></label> <input type="text" class="destext form-control" size="2" name="services[process][\'+processnum+\'][warning]" value=""> &nbsp;&nbsp; <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="\'+"'._('Critical Threshold').'"+\'"></label> <input type="text" class="destext form-control" size="2" name="services[process][\'+processnum+\'][critical]" value=""></td></tr>\');
                });

                $("#add-new-service").click(function() {
                    servicenum++;
                    $("#services-list").append(\'<tr><td><input type="checkbox" name="services[services][\'+servicenum+\'][monitor]"></td><td><input type="text" class="form-control" style="width: 240px;" name="services[services][\'+servicenum+\'][desc]" value=""></td><td><div class="input-group"><input type="text" class="form-control" style="width: 200px;" name="services[services][\'+servicenum+\'][name]" value="">'.$service_selector.'</div></td><td><label class="normal" style="margin-right: 6px;"><input name="services[services][\'+servicenum+\'][state]" type="radio" value="running" style="vertical-align: text-bottom;" checked> ' . _("Running") . '</label><label class="normal"><input type="radio" value="stopped" style="vertical-align: text-bottom;" name="services[services][\'+servicenum+\'][state]"> ' . _("Stopped") . '</label></td></tr>\');
                });

                $("#add-new-counter").click(function() {
                    counternum++;
                    $("#counter-list").append(\'<tr><td><input type="checkbox" class="checkbox" name="services[counters][\'+counternum+\'][monitor]" /></td><td style="padding-right: 10px;"><input type="text" style="width: 200px;" class="form-control" name="services[counters][\'+counternum+\'][desc]" value=""></td><td style="padding-right: 10px;"><input type="text" style="width: 200px;" class="form-control" name="services[counters][\'+counternum+\'][name]" value=""></td><td style="padding-right: 10px;"><input type="text" style="width: 50px;" class="form-control" name="services[counters][\'+counternum+\'][sleep]" value=""></td><td><label><img src="'.theme_image('error.png').'" class="tt-bind" title="\'+"'._('Warning Threshold').'"+\'"></label> <input type="text" size="5" name="services[counters][\'+counternum+\'][warning]" value="" class="form-control"> &nbsp;&nbsp; <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="\'+"'._('Critical Threshold').'"+\'"></label> <input type="text" class="destext form-control" size="5" name="services[counters][\'+counternum+\'][critical]" value=""></td></tr>\');

                });

                $("#add-new-plugin").click(function() {
                    pluginnum++;
                    $("#plugin-list").append(\'<tr><td><input type="checkbox" name="services[plugins][\'+pluginnum+\'][monitor]"></td><td><input type="text" class="form-control" style="width: 200px;" name="services[plugins][\'+pluginnum+\'][desc]" value=""></td><td><div class="input-group"><input type="text" class="form-control" style="width: 200px;" name="services[plugins][\'+pluginnum+\'][name]" value="">'.$plugin_selector.'</div></td><td><input type="text" class="form-control" style="width: 300px;" name="services[plugins][\'+pluginnum+\'][args]" value=""></td></tr>\');
                });

                $("#process-list").on("click", ".process-selector", function() {
                    var process_id = $(this).data("id");
                    var content = \'<div><h2>' . _("Processes showing on the NCPA Client") . '</h2><p>\'+"' . _("Select a process and the amount of the process currently running is next to the process name.") . '"+\'</p><div><select id="selected-process" class="form-control">' . $processes_list . '</select></div><div style="margin-top: 6px;"><button data-processid="\'+process_id+\'" class="btn btn-sm btn-primary" id="add-selected-process">\'+"' . _("Select this Process") . '"+\'</button></div></div>\';
                    
                    $("#child_popup_container").width(450);
                    $("#child_popup_layer").width(480);
                    $("#child_popup_layer").css("position", "fixed");
                    set_child_popup_content(content);
                    display_child_popup();
                });

                $("#services-list").on("click", ".service-selector", function() {
                    var service_id = $(this).data("id");
                    var content = \'<div><h2>' . _("Services Available on the NCPA Client") . '</h2><p>\'+"' . _("Select a service that is either running or stopped from the NCPA client host to atuomatically fill in the service name and the expected state.") . '"+\'</p><div><select id="selected-service" class="form-control">' . $service_list . '</select></div><div style="margin-top: 6px;"><button data-serviceid="\'+service_id+\'" class="btn btn-sm btn-primary" id="add-selected-service">\'+"' . _("Select this Service") . '"+\'</button></div></div>\';
                    
                    $("#child_popup_container").width(450);
                    $("#child_popup_layer").width(480);
                    $("#child_popup_layer").css("position", "fixed");
                    set_child_popup_content(content);
                    display_child_popup();
                });

                $("#plugin-list").on("click", ".plugin-selector", function() {
                    var plugin_id = $(this).data("id");
                    var content = \'<div><h2>\'+"' . _("Plugins Available on the NCPA Client") . '"+\'</h2><p>\'+"' . _("Select a plugin to use from the list of available plugins.") . '"+\'</p><div>' . $plugin_list . '</div><div style="margin-top: 6px;"><button data-pluginid="\'+plugin_id+\'" class="btn btn-sm btn-primary" id="add-selected-plugin">\'+"' . _("Select this Plugin") . '"+\'</button></div></div>\';
                    
                    $("#child_popup_container").width(450);
                    $("#child_popup_layer").width(480);
                    $("#child_popup_layer").css("position", "fixed");
                    set_child_popup_content(content);
                    display_child_popup();
                });

                $("#child_popup_container").on("click", "#add-selected-service", function() {
                    var service_id = $(this).data("serviceid");
                    var selected_service = $("#selected-service option:selected").val();
                    var selected_state = $("#selected-service option:selected").data("status");
                    $(\'input[name="services[services][\'+service_id+\'][desc]"]\').val(selected_service + " Service Status");
                    $(\'input[name="services[services][\'+service_id+\'][name]"]\').val(selected_service);
                    $(\'input[name="services[services][\'+service_id+\'][monitor]"]\').prop("checked", true);
                    $(\'input[name="services[services][\'+service_id+\'][state]"][value="\'+selected_state+\'"]\').prop("checked", true);
                    close_child_popup();
                });

                $("#child_popup_container").on("click", "#add-selected-process", function() {
                    var process_id = $(this).data("processid");
                    var selected_process = $("#selected-process option:selected").val();
                    var selected_amount = $("#selected-process option:selected").data("amount");
                    var warning = Math.ceil(selected_amount*1.5);
                    var critical = Math.ceil(selected_amount*2);
                    if (critical <= warning) {
                        critical++;
                    }
                    $(\'input[name="services[process][\'+process_id+\'][name]"]\').val(selected_process);
                    $(\'input[name="services[process][\'+process_id+\'][desc]"]\').val(selected_process.replace(".exe", ""));
                    $(\'input[name="services[process][\'+process_id+\'][warning]"]\').val(warning);
                    $(\'input[name="services[process][\'+process_id+\'][critical]"]\').val(critical);
                    $(\'input[name="services[process][\'+process_id+\'][monitor]"]\').prop("checked", true);
                    close_child_popup();
                });

                $("#child_popup_container").on("click", "#add-selected-plugin", function() {
                    var plugin_id = $(this).data("pluginid");
                    var selected_plugin = $("#selected-plugin option:selected").val();
                    $(\'input[name="services[plugins][\'+plugin_id+\'][desc]"]\').val("Plugin "+selected_plugin);
                    $(\'input[name="services[plugins][\'+plugin_id+\'][name]"]\').val(selected_plugin);
                    $(\'input[name="services[plugins][\'+plugin_id+\'][monitor]"]\').prop("checked", true);
                    $(\'input[name="services[plugins][\'+plugin_id+\'][args]"]\').focus();
                    close_child_popup();
                });

            });
            </script>
            ';

            if ($os == "windows") {

                $output .= '
                <h5 class="ul">' . _('Windows Counters') . '</h5>
                <p>' . _("Specify any windows counters you would like NCPA to check the value of. Per second counters require a sleep time.") . '</p>
                <table class="table table-condensed table-no-border table-auto-width table-padded">
                    <thead>
                        <tr>
                            <td></td>
                            <td><label>'._('Service Description').'</label></td>
                            <td><label>'._('Counter Name').'</label> <i class="fa fa-question-circle pop" data-content="' . _('This value can be any Windows performance counter. To view available counters run in cmd.exe').':<br><code>TypePerf.exe -q</code>"></i></td>
                            <td><label>'._('Sleep').'</label> <i class="fa fa-question-circle pop" data-content="' . _('If the counter is per second (/sec in name) then you must put a sleep value to see data. Sleep value is in seconds. Most counters are fine with 1-5 seconds.') . '"></i></td>
                            <td><label>'._('Thresholds').'</label></td>
                        </tr>
                    </thead>
                    <tbody id="counter-list">';

                foreach ($services['counters'] as $i => $metrics) {
                    $output .= '
                    <tr>
                        <td><input type="checkbox" class="checkbox" name="services[counters][' . $i . '][monitor]" ' . is_checked($metrics['monitor'], 'on') . '></td>
                        <td style="padding-right: 10px;">
                            <input type="text" style="width: 240px;" class="form-control" name="services[counters][' . $i . '][desc]" value="' . encode_form_val($metrics['desc']) . '">
                        </td>
                        <td style="padding-right: 10px;">
                            <input type="text" style="width: 300px;" class="form-control" name="services[counters][' . $i . '][name]" value="' . encode_form_val($metrics['name']) . '">
                        </td>
                        <td style="padding-right: 10px;">
                            <input type="text" style="width: 50px;" class="form-control" name="services[counters][' . $i . '][sleep]" value="' . encode_form_val($metrics['sleep']) . '">
                        </td>
                        <td>
                            <label><img src="'.theme_image('error.png').'" class="tt-bind" title="'._('Warning Threshold').'"></label>
                            <input type="text" size="5" name="services[counters][' . $i . '][warning]" value="' . encode_form_val($metrics['warning']) . '" class="form-control"> &nbsp;&nbsp;
                            <label><img src="'.theme_image('critical_small.png').'" class="tt-bind" title="'._('Critical Threshold').'"></label>
                            <input type="text" size="5" name="services[counters][' . $i . '][critical]" value="' . encode_form_val($metrics['critical']) . '" class="form-control">
                        </td>
                    </tr>';
                }

                $output .= '
                    </tbody>
                </table>
                <div style="margin: 10px 0 20px 0;">
                    <a style="cursor: pointer;" id="add-new-counter">'._('Add Another Windows Counter Check').'</a>
                </div>
                ';

            }

            $output .= '
            <h5 class="ul">' . _('Plugins') . '</h5>
            <p>' . _("If you have provided plugins on the client that you would like NCPA to run, specify them and the arguments to be passed here.") . '</p>
            <table class="table table-condensed table-no-border table-auto-width table-padded">
                <thead>
                    <tr>
                        <td></td>
                        <td><label>'._('Service Description').'</label></td>
                        <td><label>'._('Plugin Name').'</label></td>
                        <td><label>'._('Plugin Arguments').'</label> <i class="fa fa-question-circle pop" data-content="' . _('Write out all your arguments as they would be passed via the command line.') . '<br>' . _('Example').':<br><code>-w 10 -c 20 --search=\'error\'</code>"></i></td>
                    </tr>
                </thead>
                <tbody id="plugin-list">';

                foreach ($services['plugins'] as $i => $metrics) {
                    $plugin_selector = '';
                    if (empty($hosts)) {
                        $plugin_selector = '<div class="input-group-addon" style="padding-bottom: 5px;"><span style="cursor: pointer;" class="plugin-selector" data-id="' . $i . '"><img src="' . theme_image('history2.png') . '" title="' . _("Select a plugin from the list") . '"></span></div>';
                    }
                    $output .= '
                        <tr>
                            <td>
                                <input type="checkbox" class="checkbox" name="services[plugins][' . $i . '][monitor]" ' . is_checked($metrics['monitor'], 'on') . '>
                            </td>
                            <td style="padding-right: 10px;">
                                <input type="text" style="width: 200px;" class="form-control" name="services[plugins][' . $i . '][desc]" value="' . encode_form_val($metrics['desc']) . '">
                            </td>
                            <td style="padding-right: 10px;">
                                <div class="input-group">
                                    <input type="text" class="form-control" style="width: 200px;" name="services[plugins][' . $i . '][name]" value="' . encode_form_val($metrics['name']) . '">
                                    '.$plugin_selector.'
                                </div>
                            </td>
                            <td style="padding-right: 10px;">
                                <input type="text" style="width: 300px;" class="form-control" name="services[plugins][' . $i . '][args]" value="' . encode_form_val($metrics['args']) . '">
                            </td>
                        </tr>';
                }

                $output .= '</tbody>
            </table>
            <div style="margin: 10px 0 20px 0;">
                <a style="cursor: pointer;" id="add-new-plugin">'._('Add Another Plugin Check').'</a>
            </div>';
            break;

        case CONFIGWIZARD_MODE_VALIDATESTAGE2DATA:
            // Get variables that were passed to us
            $ip_address = grab_array_var($inargs, 'ip_address');
            $hostname = grab_array_var($inargs, 'hostname');
            $port = grab_array_var($inargs, 'port');
            $token = grab_array_var($inargs, 'token');
            $default_mem_units = grab_array_var($inargs, 'default_mem_units');

            $hosts = grab_array_var($inargs,  "hosts", array());
            $job_id = grab_array_var($inargs, "job_id", 0);

            // Check for errors
            $errors = 0;
            $errmsg = array();
            if (empty($hosts) && is_valid_host_name($hostname) == false) {
                $errmsg[$errors++] = "Invalid host name.";
            }

            if ($errors > 0) {
                $outargs[CONFIGWIZARD_ERROR_MESSAGES] = $errmsg;
                $result = 1;
            }
            break;

        case CONFIGWIZARD_MODE_GETSTAGE3HTML:
            // Get variables that were passed to us
            $ip_address = grab_array_var($inargs, 'ip_address');
            $hostname = grab_array_var($inargs, 'hostname');
            $port = grab_array_var($inargs, 'port');
            $token = grab_array_var($inargs, 'token');
            $services = grab_array_var($inargs, 'services', array());
            if (empty($services)) {
                $services_serial = grab_array_var($inargs, "services_serial", "");
                $services = json_decode(base64_decode($services_serial), true);
            }
            $default_mem_units = grab_array_var($inargs, 'default_mem_units');
            $os = grab_array_var($inargs, 'os', '');

            $job_id = grab_array_var($inargs, 'job_id', 0);
            $hosts = grab_array_var($inargs, 'hosts', array());

            $output = '
            <input type="hidden" name="ip_address" value="' . encode_form_val($ip_address) . '" />
            <input type="hidden" name="hostname" value="' . encode_form_val($hostname) . '" />
            <input type="hidden" name="port" value="' . encode_form_val($port) . '" />
            <input type="hidden" name="token" value="' . encode_form_val($token) . '" />
            <input type="hidden" name="os" value="' . encode_form_val($os) . '" />
            <input type="hidden" name="default_mem_units" value="' . encode_form_val($default_mem_units) . '" />
            <input type="hidden" name="services_serial" value="' . base64_encode(json_encode($services)) . '" />
            <input type="hidden" name="job_id" value="' . intval($job_id) . '" />';

            if (!empty($hosts)) {
                foreach ($hosts as $h) {
                    $output .= '<input type="hidden" name="hosts[]" value="' . $h . '" />';
                }
            }

            break;

        case CONFIGWIZARD_MODE_VALIDATESTAGE3DATA:
            break;

        case CONFIGWIZARD_MODE_GETFINALSTAGEHTML:
            // Get variables that were passed to us
            $ip_address = grab_array_var($inargs, 'ip_address');
            $hostname = grab_array_var($inargs, 'hostname');
            $port = grab_array_var($inargs, 'port');
            $token = grab_array_var($inargs, 'token');
            $services = grab_array_var($inargs, 'services', array());
            if (empty($services)) {
                $services_serial = grab_array_var($inargs, "services_serial", "");
                $services = json_decode(base64_decode($services_serial), true);
            }
            $default_mem_units = grab_array_var($inargs, 'default_mem_units');
            $os = grab_array_var($inargs, 'os', '');

            $job_id = grab_array_var($inargs, 'job_id', 0);
            $hosts = grab_array_var($inargs, 'hosts', array());

            $output = '
            <input type="hidden" name="ip_address" value="' . encode_form_val($ip_address) . '" />
            <input type="hidden" name="hostname" value="' . encode_form_val($hostname) . '" />
            <input type="hidden" name="port" value="' . encode_form_val($port) . '" />
            <input type="hidden" name="token" value="' . encode_form_val($token) . '" />
            <input type="hidden" name="os" value="' . encode_form_val($os) . '" />
            <input type="hidden" name="default_mem_units" value="' . encode_form_val($default_mem_units) . '" />
            <input type="hidden" name="services_serial" value="' . base64_encode(json_encode($services)) . '" />
            <input type="hidden" name="job_id" value="' . intval($job_id) . '" />';

            if (!empty($hosts)) {
                foreach ($hosts as $h) {
                    $output .= '<input type="hidden" name="hosts[]" value="' . $h . '" />';
                }
            }
            break;

        case CONFIGWIZARD_MODE_GETOBJECTS:
            $hostname = grab_array_var($inargs, "hostname", "");
            $ip_address = grab_array_var($inargs, "ip_address", "");
            $port = grab_array_var($inargs, "port", "");
            $token = grab_array_var($inargs, "token", "");
            $default_mem_units = grab_array_var($inargs, 'default_mem_units');
            $services_serial = grab_array_var($inargs, "services_serial", "");
            $services = json_decode(base64_decode($services_serial), true);
            $os = grab_array_var($inargs, "os", "");

            $hosts = grab_array_var($inargs,  "hosts", array());
            $job_id = grab_array_var($inargs, "job_id", 0);

            // Get job info and token
            if (!empty($job_id) && !empty($hosts)) {
                $job = deploy_get_job($job_id);
                $token = decrypt_data($job['ncpa_token']);
            }

            // Save data for later use in re-entrance
            $meta_arr = array();
            $meta_arr["hostname"] = $hostname;
            $meta_arr["ip_address"] = $ip_address;
            $meta_arr["port"] = $port;
            $meta_arr["token"] = $token;
            $meta_arr["services"] = $services;
            $meta_arr["job_id"] = $job_id;
            $meta_arr["hosts"] = $hosts;
            save_configwizard_object_meta($wizard_name, $hostname, "", $meta_arr);

            // Escape values for check_command line
            if (function_exists('nagiosccm_replace_command_line')) {
                $token = nagiosccm_replace_command_line($token, '$');
            } else {
                $token = str_replace('!', '\!', $token);
            }

            $objs = array();

            if (empty($hosts)) {

                if (!host_exists($hostname)) {
                    $objs[] = array(
                        "type" => OBJECTTYPE_HOST,
                        "use" => "xiwizard_ncpa_host",
                        "host_name" => $hostname,
                        "address" => $ip_address,
                        "icon_image" => npca_configwizard_get_os_icon($os),
                        "statusmap_image" => npca_configwizard_get_os_icon($os),
                        "_xiwizard" => $wizard_name);
                }

            } else {

                foreach ($hosts as $h) {
                    if (!host_exists($h)) {
                        $objs[] = array(
                            "type" => OBJECTTYPE_HOST,
                            "use" => "xiwizard_ncpa_host",
                            "host_name" => $h,
                            "address" => $h,
                            "_xiwizard" => $wizard_name);
                    }
                }

            }

            // Common plugin opts
            $commonopts = "-t ".escapeshellarg($token)." ";
            if ($port) {
                $commonopts .= "-P ".intval($port)." ";
            }

            // If we don't already have an array of hosts, make it
            $hostnames = array($hostname);
            if (!empty($hosts)) {
                $hostnames = $hosts;
            }

            foreach ($hostnames as $hostname) {
                foreach ($services as $type => $args) {
                    $pluginopts = "";
                    $pluginopts .= $commonopts;

                    switch ($type) {

                        case "cpu_usage":
                            if (!array_key_exists('monitor', $args)) {
                                break;
                            }
                            $pluginopts .= "-M cpu/percent";

                            if (!empty($args['warning'])) {
                                $pluginopts .= " -w " . escapeshellarg($args["warning"]);
                            }
                            if (!empty($args['critical'])) {
                                $pluginopts .= " -c " . escapeshellarg($args["critical"]);
                            }

                            if (!empty($args['average'])) {
                                $pluginopts .= " -q 'aggregate=avg'";
                            }

                            $objs[] = array(
                                "type" => OBJECTTYPE_SERVICE,
                                "host_name" => $hostname,
                                "service_description" => "CPU Usage",
                                "use" => "xiwizard_ncpa_service",
                                "check_command" => "check_xi_ncpa!" . $pluginopts,
                                "_xiwizard" => $wizard_name);
                            break;

                        case "users":
                            if (!array_key_exists('monitor', $args)) {
                                break;
                            }
                            $pluginopts .= "-M user/count";

                            if (!empty($args['warning'])) {
                                $pluginopts .= " -w " . escapeshellarg($args["warning"]);
                            }
                            if (!empty($args['critical'])) {
                                $pluginopts .= " -c " . escapeshellarg($args["critical"]);
                            }

                            $objs[] = array(
                                "type" => OBJECTTYPE_SERVICE,
                                "host_name" => $hostname,
                                "service_description" => "User Count",
                                "use" => "xiwizard_ncpa_service",
                                "check_command" => "check_xi_ncpa!" . $pluginopts,
                                "_xiwizard" => $wizard_name);
                            break;

                        case "memory_usage":
                            if (!array_key_exists('monitor', $args)) {
                                break;
                            }
                            $pluginopts .= "-M memory/virtual";

                            if (!empty($default_mem_units)) {
                                $pluginopts .= " -u " . escapeshellarg($default_mem_units);
                            }
                            if (!empty($args['warning'])) {
                                $pluginopts .= " -w " . escapeshellarg($args["warning"]);
                            }
                            if (!empty($args['critical'])) {
                                $pluginopts .= " -c " . escapeshellarg($args["critical"]);
                            }

                            $objs[] = array(
                                "type" => OBJECTTYPE_SERVICE,
                                "host_name" => $hostname,
                                "service_description" => "Memory Usage",
                                "use" => "xiwizard_ncpa_service",
                                "check_command" => "check_xi_ncpa!" . $pluginopts,
                                "_xiwizard" => $wizard_name);
                            break;

                        case "swap_usage":
                            if (!array_key_exists('monitor', $args)) {
                                break;
                            }
                            $pluginopts .= "-M memory/swap";

                            if (!empty($default_mem_units)) {
                                $pluginopts .= " -u " . escapeshellarg($default_mem_units);
                            }
                            if (!empty($args['warning'])) {
                                $pluginopts .= " -w " . escapeshellarg($args["warning"]);
                            }
                            if (!empty($args['critical'])) {
                                $pluginopts .= " -c " . escapeshellarg($args["critical"]);
                            }

                            $objs[] = array(
                                "type" => OBJECTTYPE_SERVICE,
                                "host_name" => $hostname,
                                "service_description" => "Swap Usage",
                                "use" => "xiwizard_ncpa_service",
                                "check_command" => "check_xi_ncpa!" . $pluginopts,
                                "_xiwizard" => $wizard_name);
                            break;

                        case "disk":
                            foreach ($args as $id => $metrics) {
                                if (!array_key_exists('monitor', $metrics)) {
                                    continue;
                                }

                                // Ensure we are using the right name/title for the disk API (and verify it has | instead of / or \)
                                $title = $metrics['name'];
                                if (isset($metrics['title'])) {
                                    $title = $metrics['title'];
                                }
                                $title = str_replace(array('\\', '/'), '|', $title);

                                $disk = "disk/logical/{$title}";
                                $theseopts = "{$pluginopts} -M " . escapeshellarg($disk);

                                if (!empty($metrics["warning"])) {
                                    $theseopts .= " -w " . escapeshellarg($metrics["warning"]);
                                }
                                if (!empty($metrics["critical"])) {
                                    $theseopts .= " -c " . escapeshellarg($metrics["critical"]);
                                }

                                // Make sure back slash doesn't escape service description line
                                $service_name = str_replace('\\', '/', $metrics["name"]);

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => "Disk Usage on " . $service_name,
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        case "interface":
                            foreach ($args as $id => $metrics) {
                                if (!array_key_exists('monitor', $metrics) || empty($metrics['name'])) {
                                    continue;
                                }

                                $title = $metrics['name'];

                                $theseopts = "{$pluginopts} -M 'interface/{$title}/bytes_sent' -d -u M";

                                if (!empty($metrics["warning"])) {
                                    $theseopts .= " -w " . escapeshellarg($metrics["warning"]);
                                }
                                if (!empty($metrics["critical"])) {
                                    $theseopts .= " -c " . escapeshellarg($metrics["critical"]);
                                }

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => "{$title} Bandwidth - Outbound",
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);

                                $theseopts = "{$pluginopts} -M 'interface/{$title}/bytes_recv' -d -u M";

                                if (!empty($metrics["warning"])) {
                                    $theseopts .= " -w " . escapeshellarg($metrics["warning"]);
                                }
                                if (!empty($metrics["critical"])) {
                                    $theseopts .= " -c " . escapeshellarg($metrics["critical"]);
                                }

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => "{$title} Bandwidth - Inbound",
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        case "services":
                            foreach ($args as $i => $service) {
                                if (!array_key_exists('monitor', $service) || empty($service['name']) || empty($service['desc'])) {
                                    continue;
                                }
                                $query = "service=" . str_replace('$', '$$', $service["name"]) . ",status=" . $service["state"];
                                $theseopts = "{$pluginopts} -M 'services' -q " . escapeshellarg($query);
                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => $service["desc"],
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        case "process":
                            foreach ($args as $i => $metrics) {
                                if (!array_key_exists('monitor', $metrics) || empty($metrics['desc'])) {
                                    continue;
                                }
                                $proc_name = $metrics['name'];
                                $theseopts = "{$pluginopts} -M 'processes' -q 'name={$proc_name}'";

                                if (!empty($metrics["warning"])) {
                                    $theseopts .= " -w " . escapeshellarg($metrics["warning"]);
                                }
                                if (!empty($metrics["critical"])) {
                                    $theseopts .= " -c " . escapeshellarg($metrics["critical"]);
                                }

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => $metrics['desc'],
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        case 'counters':
                            foreach ($args as $i => $counter) {
                                if (!array_key_exists('monitor', $counter) || empty($counter['desc'])) {
                                    continue;
                                }
                                $theseopts = "{$pluginopts} -M ". escapeshellarg("windowscounters/".str_replace('\\', '/', $counter['name']));

                                if (!empty($counter["warning"])) {
                                    $theseopts .= " -w " . escapeshellarg($counter["warning"]);
                                }
                                if (!empty($counter["critical"])) {
                                    $theseopts .= " -c " . escapeshellarg($counter["critical"]);
                                }

                                if (!empty($count["sleep"])) {
                                    $theseopts .= " -q 'sleep=" . intval($counter["sleep"]) . "'";
                                }

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => $counter['desc'],
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        case "plugins":
                            foreach ($args as $i => $plugin) {
                                if (!array_key_exists('monitor', $plugin) || empty($plugin['desc'])) {
                                    continue;
                                }
                                $theseopts = "{$pluginopts} -M " . escapeshellarg("plugins/".$plugin['name']);

                                if (!empty($plugin['args'])) {
                                    $theseopts .= " -q " . escapeshellarg("args=".$plugin['args']);
                                }

                                $objs[] = array(
                                    "type" => OBJECTTYPE_SERVICE,
                                    "host_name" => $hostname,
                                    "service_description" => $plugin['desc'],
                                    "use" => "xiwizard_ncpa_service",
                                    "check_command" => "check_xi_ncpa!" . $theseopts,
                                    "_xiwizard" => $wizard_name);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }

            // Return the object definitions to the wizard
            $outargs[CONFIGWIZARD_NAGIOS_OBJECTS] = $objs;
            break;

        default:
            break;
    }

    return $output;
}


/**
 * Get operating system icon based on OS name
 *
 * @param   string  $os     Operating system
 * @return  string          Icon file
 */
function npca_configwizard_get_os_icon($os)
{
    $icon = "ncpa.png";
    $os = strtolower($os);

    switch ($os) {
        case "rhel":
            $icon = "redhat.png";
            break;
        case "fedora":
            $icon = "fedora.png";
            break;
        case "centos":
            $icon = "centos.png";
            break;
        case "oracle":
            $icon = "oracle-linux.gif";
            break;
        case "ubuntu":
            $icon = "ubuntu.png";
            break;
        case "debian":
            $icon = "debian.png";
            break;
        case "sles":
            $icon = "suse_enterprise.png";
            break;
        case "suse":
            $icon = "opensuse.png";
            break;
        case "windows":
            $icon = "win_server.png";
            break;
        case "aix":
            $icon = "aix.png";
            break;
        case "sol":
            $icon = "solaris.png";
            break;
        case "osx":
            $icon = "osx.png";
            break;
    }

    return $icon;
}


/**
 * Send a request to the NCPA API and get the returned JSON as an array
 *
 * @param   string  $endpoint       API endpoint
 * @param   string  $api_url        API URL
 * @param   string  $token          Security token
 * @param   boolean $no_ssl_verify  If true, do not verify SSL certificate
 * @param   array   $params         Optional HTTP parameters passed via the URL
 * @return  array
 */
function ncpa_configwizard_get_api_data($endpoint, $api_url, $token, $no_ssl_verify, $params = array())
{
    // Remove SSL verification or not
    $context = array("ssl" => array("verify_peer" => true, "verify_peer_name" => true));
    if ($no_ssl_verify) {
        $context['ssl']['verify_peer'] = false;
        $context['ssl']['verify_peer_name'] = false;
    }

    // Setup API url
    $params['token'] = $token;
    $full_api_url = $api_url."/".$endpoint."?".http_build_query($params);

    // Do connection and get data
    $data = file_get_contents($full_api_url, false, stream_context_create($context));
    $data = json_decode($data, true);
    if (array_key_exists('value', $data)) {
        $data = array_pop($data['value']);
    } else {
        $data = array_pop($data);
    }

    return $data;
}
