<?php
require_once(dirname(__FILE__) . '/../includes/common.inc.php');
pre_init();
init_session();
check_authentication(false);
?>
<!DOCTYPE HTML>
<html>
<head> 
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
  
<script>
$(document).ready(function() {
    $('#datatable1').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": "./server_processing.php",
        dom: 'lBfrtip',
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": { "sLengthMenu": "Mostrar _MENU_ Calls ",},
        buttons: [
            {
                extend: 'copy',
                title: null,
                exportOptions: {
                    columns: ':visible'
                }
            },
            'csv',
            {
                extend: 'excel',
                filename: 'NEBR',
                title: function(){
                var d = new Date().toJSON().slice(0,10);
                return 'NEBR_' + d;},
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                filename: 'NEBR',
                title: function(){
                var d = new Date().toJSON().slice(0,10);
                return 'NEBR_' + d;},
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                filename: 'NEBR',
                title: function(){
                var d = new Date().toJSON().slice(0,10);
                return 'NEBR_' + d;},
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
  } );
} );
</script>

</head> 

<body data-spy="scroll" data-target=".navbar" data-offset="50"> 
  
<nav class="navbar navbar-fixed-top " style="background: linear-gradient(to bottom, #ccc 0%, #ccccccc4 100%);">
  <div class="container-fluid"> 
    <div class="navbar-header"> 
      <a class="navbar-brand" href="#" onClick="window.location.reload();" >NeBrasil Reload</a> 
    </div> 
  </div> 
</nav>
<div id="section1" class="container-fluid"> 
  <br><br><br>
<table id="datatable1" class="display nowrap" width="100%">
<thead>
            <tr>
                <th>Host</th>
                <th>Alias</th>
                <th>Service</th>
                <th>Information</th>
                <th>All Information</th>
            </tr>
        </thead>
</table>
</div> 
  
</body> 
</html>