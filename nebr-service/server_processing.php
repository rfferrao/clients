<?php

require_once(dirname(__FILE__) . '/../includes/common.inc.php');
pre_init();
init_session();
check_authentication(false);

//$ch = curl_init();
//
//curl_setopt($ch, CURLOPT_URL, 'https://127.0.0.1/nagios/cgi-bin/objectjson.cgi?query=servicelist');
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
//curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
//curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
//
//curl_setopt($ch, CURLOPT_USERPWD, 'nagiosadmin' . ':' . '???');
//
//$result = curl_exec($ch);
//if (curl_errno($ch)) {
//    echo 'Error:' . curl_error($ch);
//}
//curl_close($ch);

$hosts = shell_exec("/usr/bin/env REQUEST_METHOD='GET' REMOTE_USER='nagiosadmin' QUERY_STRING='query=hostlist&details=true' /usr/local/nagios/sbin/objectjson.cgi | tail -n +7");

$objhost = array(hostlist => (array) json_decode($hosts)->data->hostlist);

$result = shell_exec("/usr/bin/env REQUEST_METHOD='GET' REMOTE_USER='nagiosadmin' QUERY_STRING='query=servicelist&details=true' /usr/local/nagios/sbin/statusjson.cgi | tail -n +7");

$obj = json_decode($result);

$lista = array(data => array());

foreach ($obj->data->servicelist as $k=>$v){
    foreach ($v as $j){
        $lista["data"][] = array($k,$objhost["hostlist"][$k]->alias,$j->description,$j->plugin_output,$j->long_plugin_output);
    }
}

echo json_encode($lista);

die();
